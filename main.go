package main

import (
	"bufio"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/valyala/fasthttp"
)

func setSsnId(ctx *fasthttp.RequestCtx) {
	kuki := []byte("ssnid=01234567891123456789212345678931; SameSite=Lax; HttpOnly")
	//              012345678911234567892123456789312345678
	rand.Read(kuki[14:38])
	base64.URLEncoding.Encode(kuki[6:38], kuki[14:38])
	ctx.Response.Header.SetBytesKV([]byte("Set-Cookie"), kuki)
}
func getSsnId(ctx *fasthttp.RequestCtx) (id string, err error) {
	kuki := ctx.Request.Header.PeekBytes([]byte("Cookie"))
	kukiLen := len(kuki)
	if kukiLen == 0 {
		err = errors.New("1.Sess ID Err")
		return
	}
	i := 6
	for i < kukiLen && string(kuki[i-6:i]) != "ssnid=" {
		i++
	}
	if (i + 32) <= kukiLen {
		id = string(kuki[i : i+32])
	} else {
		err = errors.New("2.Sess ID Err")
	}
	return
}
func delSsnId(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.SetBytesKV([]byte("Set-Cookie"), []byte("ssnid=; Expires=Sat, 01 Jan 2000 00:00:00 GMT"))
}
func broker() (mssg chan string, reg func(string) chan string, unreg func(string)) {
	mssgs := map[string]chan string{}
	mssg = make(chan string, 1)
	var mtx sync.Mutex
	go func() {
		for {
			_mssg := <-mssg
			for _, __mssg := range mssgs {
				__mssg <- _mssg
			}
		}
	}()
	reg = func(id string) (mssg chan string) {
		println("reg id:", id)
		mssg = make(chan string, 1)
		mtx.Lock()
		mssgs[id] = mssg
		mtx.Unlock()
		return
	}
	unreg = func(id string) {
		mtx.Lock()
		delete(mssgs, id)
		mtx.Unlock()
	}
	return
}
func index(file *os.File) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		id, _ := getSsnId(ctx)
		if id != "" {
			ctx.Write([]byte("already opened"))
			return
		}
		setSsnId(ctx)
		ctx.SetContentTypeBytes([]byte("text/html; charset=utf-8"))
		io.Copy(ctx, file)
		file.Seek(0, 0)
	}
}
func sse(reg func(string) chan string, unreg func(string)) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		id, err := getSsnId(ctx)
		if err != nil {
			println("1.sse err:", err.Error())
			return
		}
		mssg := reg(id)
		ctx.Response.Header.SetBytesKV([]byte("Content-Type"), []byte("text/event-stream"))
		ctx.SetBodyStreamWriter(func(w *bufio.Writer) {
			defer func() {
				unreg(id)
				delSsnId(ctx)
			}()
			for w.Flush() == nil {
				if _, err = fmt.Fprintf(w, "data: %s\n\n", <-mssg); err != nil {
					break
				}
			}
		})
	}
}
func message(mssg chan string) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		mssg <- string(ctx.FormValue("mssg"))
	}
}
func cl0s3(unreg func(string)) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		id, err := getSsnId(ctx)
		if err != nil {
			println(err.Error())
			return
		}
		unreg(id)
		delSsnId(ctx)
	}
}

type R map[string]fasthttp.RequestHandler

func route(r R) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		hndlr, ok := r[string(ctx.Path())]
		if ok {
			hndlr(ctx)
		}
	}
}

func main() {
	file, err := os.Open("client/index.html")
	if err != nil {
		println(err.Error())
		return
	}
	defer file.Close()
	mssg, reg, unreg := broker()
	r := R{}
	r["/"] = index(file)
	r["/sse"] = sse(reg, unreg)
	r["/message"] = message(mssg)
	r["/close"] = cl0s3(unreg)
	fasthttp.ListenAndServe(":8080", route(r))
}
